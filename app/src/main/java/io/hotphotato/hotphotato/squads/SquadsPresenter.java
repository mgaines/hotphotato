package io.hotphotato.hotphotato.squads;


import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import hugo.weaving.DebugLog;
import io.hotphotato.hotphotato.firebase.HpDataBase;
import io.hotphotato.hotphotato.models.Squad;
import io.hotphotato.hotphotato.util.HpUtil;
import io.hotphotato.hotphotato.util.schedulers.BaseSchedulerProvider;
import rx.Observable;
import rx.Subscriber;
import rx.subscriptions.CompositeSubscription;


/**
 * Created by mgaines on 9/18/16
 */
class SquadsPresenter implements SquadsContract.Presenter {
    private final String TAG = SquadsPresenter.class.getSimpleName();

    private SquadsContract.View view;
    private HpDataBase hpDataBase;
    private BaseSchedulerProvider baseSchedulerProvider;
    private HpUtil hpUtil;
    private CompositeSubscription compositeSubscription;

    @Inject
    SquadsPresenter(SquadsContract.View view, HpDataBase hpDataBase, BaseSchedulerProvider baseSchedulerProvider, HpUtil hpUtil) {
        this.view = view;
        this.hpDataBase = hpDataBase;
        this.baseSchedulerProvider = baseSchedulerProvider;
        this.hpUtil = hpUtil;
        compositeSubscription = new CompositeSubscription();
    }

    @DebugLog
    @Override
    public void createNewSquad(String squadTitle) {
        view.showLoadingIndicator(true);
        compositeSubscription.add(
                hpDataBase.createNewSquad(squadTitle)
                        .subscribeOn(baseSchedulerProvider.io())
                        .observeOn(baseSchedulerProvider.ui())
                        .subscribe(new Subscriber<String>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {
                                view.showLoadingIndicator(false);
                                view.showErrorScreen();
                            }

                            @Override
                            public void onNext(String squadKey) {
                                view.showLoadingIndicator(false);
                                hpDataBase.addCurrentUserToSquad(squadKey);
                                String deepLink = hpUtil.buildDeepLink(squadKey);
                                view.sendDeepLink(deepLink);
                            }
                        }));

    }

    @DebugLog
    @Override
    public void getSquads() {
        view.showLoadingIndicator(true);
        compositeSubscription.add(
                hpDataBase.getSquadKeys()
                        .subscribeOn(baseSchedulerProvider.io())
                        .flatMap(strings -> {
                            if (strings.isEmpty()) {
                                view.showLoadingIndicator(false);
                                view.showEmptyDisplay();
                            }
                            return Observable.from(strings);
                        })
                        .observeOn(baseSchedulerProvider.ui())
                        .flatMap(hpDataBase::getSquad)
                        .subscribe(new Subscriber<Squad>() {
                            Map<String, Squad> squadMap = new HashMap<>();

                            @DebugLog
                            @Override
                            public void onCompleted() {
                            }

                            @DebugLog
                            @Override
                            public void onError(Throwable e) {
                                view.showLoadingIndicator(false);
                                view.showErrorScreen();
                            }

                            @DebugLog
                            @Override
                            public void onNext(Squad squad) {
                                view.showLoadingIndicator(false);
                                // I put it in a hash map so that when the database updates we don't ever get duplicate
                                // entries for the same squad.
                                squadMap.put(squad.getKey(), squad);

                                view.setSquads(squadMap);

                            }
                        }));
    }

    @Override
    public void onDestroy() {
        compositeSubscription.unsubscribe();
    }


}
