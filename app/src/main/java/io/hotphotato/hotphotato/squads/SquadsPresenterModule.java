package io.hotphotato.hotphotato.squads;


import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import io.hotphotato.hotphotato.util.HpUtil;
import rx.subjects.PublishSubject;

/**
 * Created by mgaines on 12/4/16
 */
@Module
public class SquadsPresenterModule {

    private final SquadsContract.View view;

    public SquadsPresenterModule(SquadsContract.View view) {
        this.view = view;
    }

    @Provides
    SquadsContract.View provideSquadContractView() {
        return view;
    }

    @Provides
    HpUtil providesHpUtil() {
        return new HpUtil();
    }

    @Provides
    @Named("create_squad_dialog_result")
    PublishSubject<String> providesPublishedSubject() {
        return PublishSubject.create();
    }
}
