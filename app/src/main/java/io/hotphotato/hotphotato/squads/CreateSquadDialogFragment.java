package io.hotphotato.hotphotato.squads;

import android.Manifest;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.widget.Button;

import io.hotphotato.hotphotato.R;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by mgaines on 1/3/17
 */

public class CreateSquadDialogFragment extends DialogFragment {
    private static final int RC_SEND_SMS = 100;
    AlertDialog alertDialog;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(R.layout.new_squad_dialog_fragment)
                .setMessage("Create New Squad")

                .setPositiveButton("Create", (dialog, id) -> {
                    //Overwritten in onStart();
                })
                .setNegativeButton("Cancel", (dialog, id) -> {
                    // User cancelled the dialog
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }

    @Override
    public void onStart() {
        super.onStart();
        alertDialog = (AlertDialog) getDialog();
        if (alertDialog != null) {
            Button positiveButton = alertDialog.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(view -> create());
        }
    }

    @AfterPermissionGranted(RC_SEND_SMS)
    public void create() {
        // Check that we have permission to send SMS messages.
        String perm = Manifest.permission.SEND_SMS;
        if (!EasyPermissions.hasPermissions(getActivity(), perm)) {
            EasyPermissions.requestPermissions(this, "Hot Photato requires SMS permissions to send invites!", RC_SEND_SMS, perm);
            return;
        }

        alertDialog.dismiss();


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);

    }

}
