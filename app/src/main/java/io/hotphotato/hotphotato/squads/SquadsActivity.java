package io.hotphotato.hotphotato.squads;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Toast;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import hugo.weaving.DebugLog;
import io.hotphotato.hotphotato.HpApplication;
import io.hotphotato.hotphotato.R;
import io.hotphotato.hotphotato.models.Squad;
import io.hotphotato.hotphotato.result.SquadResultActivity;
import io.hotphotato.hotphotato.squadhome.SquadSubmitActivity;
import io.hotphotato.hotphotato.squadvote.SquadVoteActivity;
import io.hotphotato.hotphotato.util.HpUtil;
import pub.devrel.easypermissions.EasyPermissions;
import rx.subjects.PublishSubject;

public class SquadsActivity extends AppCompatActivity implements SquadsContract.View, View.OnClickListener {

    private static final String TAG = SquadsActivity.class.getSimpleName();
    private static final int RC_SEND_SMS = 100;
    public static final String SELECTED_SQUAD_KEY = "selected_squad";
    private String deepLink;

    @Inject
    SquadsPresenter presenter;

    @Inject
    @Named("create_squad_dialog_result")
    PublishSubject<String> dialogResultObservable;

    private RecyclerView groupsRV;
    private SquadsAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ContentLoadingProgressBar loadingIndicator;


    @DebugLog
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_squads);

        findViewById(R.id.squad_home_take_photo_fab).setOnClickListener(this);

        DaggerSquadsComponent.builder()
                .applicationComponent(((HpApplication) getApplication()).getApplicationComponent())
                .squadsPresenterModule(new SquadsPresenterModule(this)).build()
                .inject(this);


        layoutManager = new LinearLayoutManager(this);
        adapter = new SquadsAdapter(this, squadItemListener);
        groupsRV = (RecyclerView) findViewById(R.id.groups_recycler_view);
        groupsRV.setLayoutManager(layoutManager);
        groupsRV.setAdapter(adapter);

        loadingIndicator = (ContentLoadingProgressBar) findViewById(R.id.loading_indicator);
        presenter.getSquads();

    }

    @DebugLog
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.squad_home_take_photo_fab:
                new CreateSquadDialogFragment().show(getSupportFragmentManager(), "tag");
                //presenter.createNewSquad("Matt's Minions");
                break;
            default:
        }
    }


    @DebugLog
    @Override
    public void sendDeepLink(String deepLink) {

        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage("8069281424", null, deepLink, null, null);
            Toast.makeText(getApplicationContext(), "Message Sent",
                    Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage().toString(),
                    Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }

        //        Intent intent = new Intent(Intent.ACTION_SEND);
        //        intent.setType("text/plain");
        //        intent.putExtra(Intent.EXTRA_SUBJECT, "Firebase Deep Link");
        //        intent.putExtra(Intent.EXTRA_TEXT, "Play Hot Photato with me!\n" + deepLink);
        //        startActivity(intent);
    }

    @DebugLog
    @Override
    public void setSquads(Map squadMap) {
        adapter.setSquads(squadMap);

    }

    @DebugLog
    @Override
    public void showLoadingIndicator(boolean showIndicator) {
        if (showIndicator) {
            loadingIndicator.show();
        } else {
            loadingIndicator.hide();
        }
    }

    @Override
    public void showEmptyDisplay() {
        //TODO Implement empty state
        Toast.makeText(this, "Empty - No Squads", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showErrorScreen() {
        //TODO Implement Error Screen
        Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
    }


    private SquadItemListener squadItemListener = (squad, itemView) -> {

        switch (HpUtil.getStage(squad)) {
            case SUBMIT:
                startSquadSubmitActivity(squad, itemView);
                break;
            case VOTE:
                startSquadVoteActitivy(squad);
                break;
            case RESULT:
                startActivity(new Intent(this, SquadResultActivity.class));
                break;
            default:
                break;
        }


    };

    @DebugLog
    private void startSquadSubmitActivity(Squad squad, View itemView) {
        Intent intent = new Intent(SquadsActivity.this, SquadSubmitActivity.class);
        intent.putExtra(SELECTED_SQUAD_KEY, squad.getKey());

        //TODO make dynamic based on number of members.

        ActivityOptionsCompat options;
        if (squad.getMembers().size() > 1) {
            Pair<View, String> p1 = Pair.create(itemView.findViewById(R.id.squad_row_portrait), "image1");
            Pair<View, String> p2 = Pair.create(itemView.findViewById(R.id.squad_row_portrait2), "image2");
            options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, p1, p2);
        } else {
            options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, itemView.findViewById(R.id.squad_row_portrait), "image1");
        }
        startActivity(intent, options.toBundle());

    }

    @DebugLog
    private void startSquadVoteActitivy(Squad squad) {
        Intent intent = new Intent(SquadsActivity.this, SquadVoteActivity.class);
        intent.putExtra(SELECTED_SQUAD_KEY, squad.getKey());
        startActivity(intent);

    }

    public interface SquadItemListener {
        void onSquadClick(Squad squad, View itemView);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
}
