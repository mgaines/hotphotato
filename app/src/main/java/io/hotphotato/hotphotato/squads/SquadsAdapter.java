package io.hotphotato.hotphotato.squads;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.hotphotato.hotphotato.R;
import io.hotphotato.hotphotato.models.Squad;
import io.hotphotato.hotphotato.models.User;
import io.hotphotato.hotphotato.util.RoundImageTransform;


/**
 * Created by mgaines on 10/4/16
 */
public class SquadsAdapter extends android.support.v7.widget.RecyclerView.Adapter<SquadsAdapter.SquadViewHolder> {

    private List<Squad> squads;
    private Context context;
    public SquadsActivity.SquadItemListener squadItemListener;

    public SquadsAdapter(Context context, SquadsActivity.SquadItemListener squadItemListener) {
        this.context = context;
        squads = new ArrayList<>();
        this.squadItemListener = squadItemListener;
    }

    @Override
    public SquadViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.squad_row, parent, false);
        return new SquadViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SquadViewHolder holder, int position) {
        Squad squad = squads.get(position);

        if (squad.getMembers().isEmpty()) {
            return;
        }

        for (User user : squad.getMembers()) {
            //TODO implement dynamic image view additions
        }


        //Temporary until above TODO is done

        if (squad.getMembers().get(0).getProfilePhotoUrl() != null) {

            Glide.with(context)
                    .load(squad.getMembers().get(0).getProfilePhotoUrl())
                    .centerCrop()
                    .crossFade()
                    .bitmapTransform(new RoundImageTransform(context))
                    .into(holder.imageView);
        } else {
            Glide.clear(holder.imageView);
        }

        if (squad.getMembers().size() > 1 && squad.getMembers().get(1).getProfilePhotoUrl() != null) {
            Glide.with(context)
                    .load(squad.getMembers().get(1).getProfilePhotoUrl())
                    .bitmapTransform(new RoundImageTransform(context))
                    .into(holder.imageView2);
        } else {
            Glide.clear(holder.imageView2);
        }
    }

    @Override
    public int getItemCount() {
        return squads.size();
    }

    public void setSquads(Map squadMap) {
        squads.clear();

        Map<String, Squad> hm = squadMap;

        for (Map.Entry<String, Squad> entry : hm.entrySet()) {
            squads.add(entry.getValue());
        }
        notifyDataSetChanged();
    }


    class SquadViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imageView;
        ImageView imageView2;

        SquadViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.squad_row_portrait);
            imageView2 = (ImageView) itemView.findViewById(R.id.squad_row_portrait2);
            itemView.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            squadItemListener.onSquadClick(squads.get(getAdapterPosition()), itemView);
        }
    }
}
