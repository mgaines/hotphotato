package io.hotphotato.hotphotato.squads;


import dagger.Component;
import io.hotphotato.hotphotato.ApplicationComponent;
import io.hotphotato.hotphotato.util.ActivityScoped;

/**
 * Created by mgaines on 12/4/16
 */
@ActivityScoped
@Component(dependencies = ApplicationComponent.class, modules = SquadsPresenterModule.class)
public interface SquadsComponent {

    void inject(SquadsActivity activity);
}
