package io.hotphotato.hotphotato.squads;

import java.util.Map;

/**
 * Created by mgaines on 9/18/16
 */
public interface SquadsContract {

    interface View {

        void sendDeepLink(String deepLink);

        void setSquads(Map squadMap);

        void showLoadingIndicator(boolean showIndicator);

        void showEmptyDisplay();

        void showErrorScreen();
    }

    interface Presenter {
        void createNewSquad(String squadTitle);

        void getSquads();

        void onDestroy();

    }
}
