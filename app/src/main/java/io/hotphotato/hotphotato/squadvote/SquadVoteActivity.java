package io.hotphotato.hotphotato.squadvote;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import javax.inject.Inject;

import io.hotphotato.hotphotato.HpApplication;
import io.hotphotato.hotphotato.R;
import io.hotphotato.hotphotato.models.Squad;
import io.hotphotato.hotphotato.squads.SquadsActivity;

/**
 * Created by mgaines on 11/14/16
 */

public class SquadVoteActivity extends AppCompatActivity implements SquadVoteContract.View {

    @Inject
    SquadVotePresenter presenter;

    private static String squadKey;

    private ImageView imageView1;
    private ImageView imageView2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_squad_vote);

        DaggerSquadVoteComponent.builder()
                .applicationComponent(((HpApplication) getApplication()).getApplicationComponent())
                .squadVotePresenterModule(new SquadVotePresenterModule(this)).build()
                .inject(this);
        squadKey = getIntent().getStringExtra(SquadsActivity.SELECTED_SQUAD_KEY);

        imageView1 = (ImageView) findViewById(R.id.image1);
        imageView2 = (ImageView) findViewById(R.id.image2);
        presenter.getSquad(squadKey);
    }

    @Override
    public void updateSquad(Squad squad) {


        //TODO make dynamic based on number of members.

        Glide.with(this)
                .load(squad.getMembers().get(0).getEntryUrl())
                .centerCrop()
                .crossFade()
                .into(imageView1);

        if (squad.getMembers().size() > 1) {
            Glide.with(this)
                    .load(squad.getMembers().get(1).getEntryUrl())
                    .centerCrop()
                    .crossFade()
                    .into(imageView2);
        }


    }
}
