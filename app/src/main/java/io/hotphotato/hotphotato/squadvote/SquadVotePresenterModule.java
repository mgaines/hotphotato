package io.hotphotato.hotphotato.squadvote;

import dagger.Module;
import dagger.Provides;

/**
 * Created by mgaines on 12/7/16
 */
@Module
public class SquadVotePresenterModule {

    private final SquadVoteContract.View view;

    public SquadVotePresenterModule(SquadVoteContract.View view) {
        this.view = view;
    }

    @Provides
    SquadVoteContract.View providesSquadVoteView() {
        return view;
    }
}
