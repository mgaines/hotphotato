package io.hotphotato.hotphotato.squadvote;


import dagger.Component;
import io.hotphotato.hotphotato.ApplicationComponent;
import io.hotphotato.hotphotato.util.ActivityScoped;

/**
 * Created by mgaines on 12/7/16
 */
@ActivityScoped
@Component(dependencies = ApplicationComponent.class, modules = SquadVotePresenterModule.class)
public interface SquadVoteComponent {

    void inject(SquadVoteActivity activity);
}
