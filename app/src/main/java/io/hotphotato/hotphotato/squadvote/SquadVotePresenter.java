package io.hotphotato.hotphotato.squadvote;


import javax.inject.Inject;

import hugo.weaving.DebugLog;
import io.hotphotato.hotphotato.firebase.HpDataBase;
import io.hotphotato.hotphotato.models.Squad;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by mgaines on 9/18/16
 */
class SquadVotePresenter implements SquadVoteContract.Presenter {

    private SquadVoteContract.View view;
    private HpDataBase hpDataBase;
    private CompositeSubscription compositeSubscription;

    @Inject
    SquadVotePresenter(SquadVoteContract.View view, HpDataBase hpDataBase) {
        this.view = view;
        this.hpDataBase = hpDataBase;
        compositeSubscription = new CompositeSubscription();
    }

    @DebugLog
    @Override
    public void getSquad(String squadKey) {
        compositeSubscription.add(
                hpDataBase.getSquad(squadKey)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<Squad>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onNext(Squad squad) {
                                view.updateSquad(squad);
                            }
                        }));
    }

    @DebugLog
    @Override
    public void onDestroy() {
        compositeSubscription.unsubscribe();
    }

}
