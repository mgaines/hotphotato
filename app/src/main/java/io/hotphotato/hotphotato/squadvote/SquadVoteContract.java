package io.hotphotato.hotphotato.squadvote;


import io.hotphotato.hotphotato.models.Squad;

/**
 * Created by mgaines on 9/18/16
 */
public interface SquadVoteContract {

    interface View {

        void updateSquad(Squad squad);



    }

    interface Presenter {


        void onDestroy();

        void getSquad(String squadKey);
    }
}
