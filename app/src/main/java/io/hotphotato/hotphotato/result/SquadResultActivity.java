package io.hotphotato.hotphotato.result;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import io.hotphotato.hotphotato.R;


/**
 * Created by mgaines on 11/14/16
 */

public class SquadResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_squad_result);
    }
}
