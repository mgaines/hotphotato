package io.hotphotato.hotphotato.util;

/**
 * Created by mgaines on 11/26/16
 */

public enum Stage {
    SUBMIT,
    VOTE,
    RESULT;
}
