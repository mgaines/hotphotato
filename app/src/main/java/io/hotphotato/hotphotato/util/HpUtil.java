package io.hotphotato.hotphotato.util;

import android.net.Uri;
import android.support.annotation.NonNull;

import hugo.weaving.DebugLog;
import io.hotphotato.hotphotato.models.Squad;
import io.hotphotato.hotphotato.models.User;

/**
 * Created by mgaines on 11/26/16
 */

public class HpUtil {

    @DebugLog
    public static Stage getStage(Squad squad) {

        if (allUsersSubmitted(squad)) {
            return Stage.VOTE;
        }

        return Stage.SUBMIT;
    }

    @DebugLog
    public static String stringObjectNotNull(Object stringObject) {
        if (stringObject == null) {
            return "";
        } else return stringObject.toString();
    }

    @DebugLog
    private static boolean allUsersSubmitted(Squad squad) {
        for (User user : squad.getMembers()) {
            if (user.getEntryUrl() == null || user.getEntryUrl().isEmpty()) {
                return false;
            }
        }
        return true;
    }


    @DebugLog
    public String buildDeepLink(@NonNull String deepLink) {
        // Values from the app's Firebase console.
        final String appCode = "hr63e";
        final String packageName = "io.hotphotato.hotphotato";
        final String url = "https://hotphotato.hotphotato.io/";


        // Build the link with all required parameters
        Uri.Builder builder = new Uri.Builder()
                .scheme("https")
                .authority(appCode + ".app.goo.gl")
                .path("/")
                .appendQueryParameter("link", url + deepLink)
                .appendQueryParameter("apn", packageName);


        // Return the completed deep link.
        return builder.build().toString();
    }
}
