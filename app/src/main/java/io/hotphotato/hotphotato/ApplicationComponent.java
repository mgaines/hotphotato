package io.hotphotato.hotphotato;


import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;
import io.hotphotato.hotphotato.firebase.HpDataBase;
import io.hotphotato.hotphotato.util.schedulers.BaseSchedulerProvider;
import rx.subjects.PublishSubject;

/**
 * Created by mgaines on 12/4/16
 */

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

    HpDataBase getHpDataBase();

    BaseSchedulerProvider getBaseSchedulerProvider();

    @Named("upload_result")
    PublishSubject<Boolean> getPublishSubject();


}
