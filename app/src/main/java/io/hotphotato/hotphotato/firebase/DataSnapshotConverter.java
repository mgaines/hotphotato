package io.hotphotato.hotphotato.firebase;

import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.List;

import io.hotphotato.hotphotato.models.Squad;
import io.hotphotato.hotphotato.models.User;
import io.hotphotato.hotphotato.util.HpUtil;

import static io.hotphotato.hotphotato.firebase.HpDataBase.MEMBERS_KEY;
import static io.hotphotato.hotphotato.firebase.HpDataBase.SQUADS_KEY;
import static io.hotphotato.hotphotato.firebase.HpDataBase.SQUAD_TITLE_KEY;
import static io.hotphotato.hotphotato.firebase.HpDataBase.SQUAD_TOPIC_KEY;
import static io.hotphotato.hotphotato.firebase.HpDataBase.SQUAD_USER_ENTRY_KEY;
import static io.hotphotato.hotphotato.firebase.HpDataBase.USERS_DISPLAY_NAME_KEY;
import static io.hotphotato.hotphotato.firebase.HpDataBase.USERS_PROFILE_PHOTO_KEY;


/**
 * Created by mgaines on 11/5/16
 */

class DataSnapshotConverter {

    static Squad toSquad(DataSnapshot squadSnapshot) {
        String squadKey = squadSnapshot.getKey();
        String squadTitle = HpUtil.stringObjectNotNull(squadSnapshot.child(SQUAD_TITLE_KEY).getValue());
        String squadTopic = HpUtil.stringObjectNotNull(squadSnapshot.child(SQUAD_TOPIC_KEY).getValue());
        Squad squad = new Squad(squadKey, squadTitle, squadTopic);
        for (DataSnapshot memberSnapshot : squadSnapshot.child(MEMBERS_KEY).getChildren()) {
            String userKey = memberSnapshot.getKey();
            String userDisplayName = HpUtil.stringObjectNotNull(memberSnapshot.child(USERS_DISPLAY_NAME_KEY).getValue());
            String userProfilePhotoUrl = HpUtil.stringObjectNotNull(memberSnapshot.child(USERS_PROFILE_PHOTO_KEY).getValue());
            String userEntryUrl = HpUtil.stringObjectNotNull(memberSnapshot.child(SQUAD_USER_ENTRY_KEY).getValue());
            User user = new User(userKey, userDisplayName, userProfilePhotoUrl, userEntryUrl);
            squad.addMember(user);
        }
        return squad;
    }

    static List<String> toSquadKeys(DataSnapshot userSnapshot) {
        List<String> squadKeys = new ArrayList<>();
        for (DataSnapshot childSnapshot : userSnapshot.child(SQUADS_KEY).getChildren()) {
            squadKeys.add(childSnapshot.getKey());
        }
        return squadKeys;
    }

    static List<String> toTopics(DataSnapshot topicsSnapshot) {
        List<String> topics = new ArrayList<>();
        for (DataSnapshot childSnapshot : topicsSnapshot.getChildren()) {
            topics.add(Integer.parseInt(childSnapshot.getKey()), childSnapshot.getValue().toString());
        }
        return topics;
    }
}
