package io.hotphotato.hotphotato.firebase;

import android.net.Uri;
import android.util.Log;

import com.ezhome.rxfirebase2.database.RxFirebaseDatabase;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hugo.weaving.DebugLog;
import io.hotphotato.hotphotato.models.Squad;
import rx.Observable;

/**
 * Every client connected to a Firebase database maintains its own internal version
 * of any active data. When data is written, it's written to this local version first.
 * <p>
 * The Firebase client then synchronizes that data with the remote database servers and
 * with other clients on a "best-effort" basis.
 * <p>
 * As a result, all writes to the database trigger local events immediately, before any
 * data is written to the server. This means your app remains responsive regardless of
 * network latency or connectivity.
 * <p>
 * Once connectivity is reestablished, your app receives the appropriate set of events
 * so that the client syncs with the current server state, without having to write any
 * custom code.
 */

public class HpDataBase {

    private static final String TAG = HpDataBase.class.getSimpleName();

    private DatabaseReference usersRef;
    private DatabaseReference squadsRef;
    private DatabaseReference topicsRef;

    static final String USERS_KEY = "users";
    static final String USERS_DISPLAY_NAME_KEY = "displayName";
    static final String USERS_PROFILE_PHOTO_KEY = "profilePhotoUrl";

    static final String SQUADS_KEY = "squads";
    static final String MEMBERS_KEY = "members";
    static final String SQUAD_TITLE_KEY = "title";
    static final String SQUAD_TOPIC_KEY = "topic";
    static final String SQUAD_STAGE_KEY = "stage";

    static final String SQUAD_USER_ENTRY_KEY = "entry";

    static final String TOPICS_KEY = "topics";


    @DebugLog
    public HpDataBase() {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        usersRef = ref.child(USERS_KEY);
        squadsRef = ref.child(SQUADS_KEY);
        topicsRef = ref.child(TOPICS_KEY);
    }


    @DebugLog
    public void updateUser() {
        // In order to run this in parallel with adding to squads I need to check for display name to know for sure if
        // the user has been created.

        // Using updateChildren here so that other operations like adding squads don't
        // have to wait for this to complete.
        Map<String, Object> user = new HashMap<>();
        if (getLoggedInUser().getDisplayName() != null) {
            user.put(USERS_DISPLAY_NAME_KEY, getLoggedInUser().getDisplayName());
        }
        if (getLoggedInUser().getPhotoUrl() != null) {
            user.put(USERS_PROFILE_PHOTO_KEY, getLoggedInUser().getPhotoUrl().toString());
        }
        usersRef.child(getLoggedInUserId()).updateChildren(user);
    }


    @DebugLog
    public Observable<String> createNewSquad(String squadTitle) {
        if (isUserLoggedIn()) {
            Map<String, Object> squadMap = new HashMap<>(2);
            squadMap.put(SQUAD_TITLE_KEY, squadTitle);
            squadMap.put(SQUAD_TOPIC_KEY, "");
            return RxFirebaseDatabase.getInstance().observeSetValuePush(squadsRef, squadMap);
        }
        return null;
    }

    @DebugLog
    public void addCurrentUserToSquad(final String squadKey) {
        if (isUserLoggedIn()) {

            // We need to track this in both the squads and users databases.

            squadsRef.child(squadKey).child(MEMBERS_KEY).child(getLoggedInUserId())
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            // If user isn't a member of the squad, add user to squad.
                            if (dataSnapshot.getValue() == null) {
                                // Update Children only takes type Map
                                Map<String, Object> user = new HashMap<>(2);
                                user.put(USERS_DISPLAY_NAME_KEY, getLoggedInUser().getDisplayName());
                                final Uri photoUri = getLoggedInUser().getPhotoUrl();
                                if (photoUri != null) {
                                    user.put(USERS_PROFILE_PHOTO_KEY, photoUri.toString());
                                }
                                squadsRef.child(squadKey).child(MEMBERS_KEY).child(getLoggedInUserId());
                                squadsRef.child(squadKey).child(MEMBERS_KEY).child(getLoggedInUserId()).updateChildren(user);

                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.d(TAG, "addCurrentUserToSquad error: ", databaseError.toException());
                        }
                    });

            usersRef.child(getLoggedInUserId()).child(SQUADS_KEY).child(squadKey)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            // If squad doesn't exist under user, add the squad to user.
                            if (dataSnapshot.getValue() == null) {
                                // Update Children only takes type Map
                                Map<String, Object> squad = new HashMap<>(1);
                                squad.put(squadKey, true);
                                usersRef.child(getLoggedInUserId()).child(SQUADS_KEY).updateChildren(squad);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.d(TAG, "addCurrentUserToSquad error: ", databaseError.toException());

                        }
                    });

        }

    }

    @DebugLog
    public Observable<List<String>> getSquadKeys() {
        if (isUserLoggedIn()) {
            return RxFirebaseDatabase.getInstance().observeValueEvent(usersRef.child(getLoggedInUserId()))
                    .map(DataSnapshotConverter::toSquadKeys);
        }
        return null;
    }

    @DebugLog
    public Observable<Squad> getSquad(String squadKey) {
        if (isUserLoggedIn()) {
            return RxFirebaseDatabase.getInstance().observeValueEvent(squadsRef.child(squadKey))
                    .map(DataSnapshotConverter::toSquad);
        }
        return null;
    }

    @DebugLog
    public Observable<List<String>> getTopics() {
        return RxFirebaseDatabase.getInstance().observeSingleValue(topicsRef)
                .map(DataSnapshotConverter::toTopics);
    }

    @DebugLog
    public boolean isUserLoggedIn() {
        return FirebaseAuth.getInstance().getCurrentUser() != null;
    }

    @DebugLog
    public String getLoggedInUserId() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    @DebugLog
    public FirebaseUser getLoggedInUser() {
        return FirebaseAuth.getInstance().getCurrentUser();
    }

    @DebugLog
    public void setTopic(String topic, String squadKey) {
        if (isUserLoggedIn()) {
            squadsRef.child(squadKey).child(SQUAD_TOPIC_KEY).setValue(topic);
        }
    }

    @DebugLog
    public void setPhotoEntry(String squadKey, String downloadUrl) {
        if (isUserLoggedIn()) {
            squadsRef.child(squadKey).child(MEMBERS_KEY).child(getLoggedInUserId()).child(SQUAD_USER_ENTRY_KEY).setValue(downloadUrl);
        }
    }
}
