package io.hotphotato.hotphotato.login;


import dagger.Component;
import io.hotphotato.hotphotato.ApplicationComponent;
import io.hotphotato.hotphotato.util.ActivityScoped;

/**
 * Created by mgaines on 12/4/16
 */
@ActivityScoped
@Component(dependencies = ApplicationComponent.class, modules = LoginPresenterModule.class)
public interface LoginComponent {

    void inject(LoginActivity activity);
}
