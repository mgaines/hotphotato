package io.hotphotato.hotphotato.login;

/**
 * Created by mgaines on 12/22/16
 */

public interface LoginContract {
    interface View {

        void processDeepLink();

        void displaySignIn();
    }

    interface Presenter {
        void login();

        void addUserToSquad(String squadKey);
    }
}
