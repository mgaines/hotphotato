package io.hotphotato.hotphotato.login;

import dagger.Module;
import dagger.Provides;

/**
 * Created by mgaines on 12/22/16
 */

@Module
public class LoginPresenterModule {

    private LoginContract.View view;

    LoginPresenterModule(LoginContract.View view) {
        this.view = view;
    }

    @Provides
    LoginContract.View providesLoginContractView() {
        return view;
    }
}
