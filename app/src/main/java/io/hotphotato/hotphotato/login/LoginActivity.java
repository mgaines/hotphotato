package io.hotphotato.hotphotato.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.appinvite.AppInvite;
import com.google.android.gms.appinvite.AppInviteReferral;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.Arrays;

import javax.inject.Inject;

import hugo.weaving.DebugLog;
import io.hotphotato.hotphotato.HpApplication;
import io.hotphotato.hotphotato.R;
import io.hotphotato.hotphotato.squads.SquadsActivity;


/**
 * Created by mgaines on 9/18/16
 */
public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, LoginContract.View {

    private final static String TAG = LoginActivity.class.getSimpleName();
    private static final int RC_SIGN_IN = 0;
    private GoogleApiClient googleApiClient;


    @Inject
    LoginPresenter loginPresenter;

    @DebugLog
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DaggerLoginComponent.builder()
                .applicationComponent(((HpApplication) getApplication()).getApplicationComponent())
                .loginPresenterModule(new LoginPresenterModule(this))
                .build()
                .inject(this);

        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(AppInvite.API)
                .build();

        loginPresenter.login();
    }

    @DebugLog
    @Override
    public void displaySignIn() {
        //TODO implement email, twitter, facebook login.
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setProviders(Arrays.asList(
                                //new AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build(),
                                //new AuthUI.IdpConfig.Builder(AuthUI.TWITTER_PROVIDER).build(),
                                //new AuthUI.IdpConfig.Builder(AuthUI.FACEBOOK_PROVIDER).build(),
                                new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build()))
                        .build(),
                RC_SIGN_IN);
    }

    @DebugLog
    @Override
    public void processDeepLink() {
        //Need to check for a deep link and process accordingly
        AppInvite.AppInviteApi.getInvitation(googleApiClient, this, false)
                .setResultCallback(
                        result -> {
                            if (result.getStatus().isSuccess()) {
                                // Extract deep link from Intent
                                Intent intent = result.getInvitationIntent();
                                String deepLink = AppInviteReferral.getDeepLink(intent);

                                // Handle the deep link.

                                Log.d(TAG, deepLink);
                                String squadKey = deepLink.substring(deepLink.lastIndexOf("/") + 1);

                                loginPresenter.addUserToSquad(squadKey);

                                startActivity(new Intent(LoginActivity.this, SquadsActivity.class));
                                // prevents the transition animation when the user starts the app.
                                overridePendingTransition(0, 0);
                                finish();
                            } else {
                                Log.d(TAG, "No deep link found.");
                                startActivity(new Intent(LoginActivity.this, SquadsActivity.class));
                                // prevents the transition animation when the user starts the app.
                                overridePendingTransition(0, 0);
                                finish();
                            }
                        });
    }

    @DebugLog
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            if (resultCode == RESULT_OK) {
                // user is signed in!
                loginPresenter.login();
            } else {
                Log.d(TAG, "User sign in failed");
            }
        }
    }

    @DebugLog
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.w(TAG, "onConnectionFailed:" + connectionResult);
        Toast.makeText(this, "Google Play Services Error: " + connectionResult.getErrorCode(),
                Toast.LENGTH_SHORT).show();
    }
}
