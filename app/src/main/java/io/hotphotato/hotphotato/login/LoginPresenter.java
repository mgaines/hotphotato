package io.hotphotato.hotphotato.login;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import io.hotphotato.hotphotato.firebase.HpDataBase;

/**
 * Created by mgaines on 12/22/16
 */

class LoginPresenter implements LoginContract.Presenter {

    private LoginContract.View view;
    private HpDataBase hpDataBase;

    @Inject
    LoginPresenter(LoginContract.View view, HpDataBase hpDataBase) {
        this.view = view;
        this.hpDataBase = hpDataBase;
    }

    @Override
    public void login() {
        if (hpDataBase.isUserLoggedIn()) {
            // This currently updates the user on every login in case the user
            // has changed their profile picture or display name
            hpDataBase.updateUser();
            view.processDeepLink();
        } else {
            view.displaySignIn();
        }
    }

    @Override
    public void addUserToSquad(@NonNull String squadKey) {
        hpDataBase.addCurrentUserToSquad(squadKey);
    }
}
