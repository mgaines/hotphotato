package io.hotphotato.hotphotato;

import android.content.Context;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.hotphotato.hotphotato.firebase.HpDataBase;
import io.hotphotato.hotphotato.util.schedulers.BaseSchedulerProvider;
import io.hotphotato.hotphotato.util.schedulers.SchedulerProvider;
import rx.subjects.PublishSubject;

/**
 * Created by mgaines on 12/4/16
 */
@Module
public class ApplicationModule {

    private final Context context;

    ApplicationModule(Context context) {
        this.context = context;
    }

    @Provides
    Context provideContext() {
        return context;
    }

    @Singleton
    @Provides
    HpDataBase provideHpDatabase() {
        return new HpDataBase();
    }

    @Singleton
    @Provides
    BaseSchedulerProvider provideBaseSchedulerProvider(){
        return SchedulerProvider.getInstance();
    }

    @Singleton
    @Provides @Named("upload_result")
    PublishSubject<Boolean> providesPublishedSubject() {
        return PublishSubject.create();
    }
}
