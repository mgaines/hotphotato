package io.hotphotato.hotphotato.squadhome;


import dagger.Component;
import io.hotphotato.hotphotato.ApplicationComponent;
import io.hotphotato.hotphotato.util.ActivityScoped;

/**
 * Created by mgaines on 12/7/16
 */
@ActivityScoped
@Component(dependencies = ApplicationComponent.class, modules = SquadSubmitPresenterModule.class)
public interface SquadSubmitComponent {

    void inject(SquadSubmitActivity activity);
}
