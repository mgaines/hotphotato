package io.hotphotato.hotphotato.squadhome;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Named;

import hugo.weaving.DebugLog;
import io.hotphotato.hotphotato.HpApplication;
import io.hotphotato.hotphotato.R;
import io.hotphotato.hotphotato.models.Squad;
import io.hotphotato.hotphotato.result.SquadResultActivity;
import io.hotphotato.hotphotato.squadvote.SquadVoteActivity;
import io.hotphotato.hotphotato.upload.UploadService;
import io.hotphotato.hotphotato.util.HpUtil;
import io.hotphotato.hotphotato.util.RoundImageTransform;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import rx.subjects.PublishSubject;

import static io.hotphotato.hotphotato.squads.SquadsActivity.SELECTED_SQUAD_KEY;


public class SquadSubmitActivity extends AppCompatActivity implements SquadSubmitContract.View, View.OnClickListener {

    private static final String TAG = SquadSubmitActivity.class.getSimpleName();


    private static String squadKey;
    private Button startButton;
    private FloatingActionButton fab;
    private TextView topicTextView;

    private ImageView imageView1;
    private ImageView imageView2;

    @Inject
    @Named("upload_result")
    PublishSubject<Boolean> uploadObservable;

    @Inject
    SquadSubmitPresenter presenter;

    @DebugLog
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerSquadSubmitComponent.builder()
                .applicationComponent(((HpApplication) getApplication()).getApplicationComponent())
                .squadSubmitPresenterModule(new SquadSubmitPresenterModule(this))
                .build()
                .inject(this);

        setContentView(R.layout.activity_squad_submit);
        squadKey = getIntent().getStringExtra(SELECTED_SQUAD_KEY);

        startButton = (Button) findViewById(R.id.squad_details_start_button);
        startButton.setOnClickListener(this);
        fab = (FloatingActionButton) findViewById(R.id.squad_home_take_photo_fab);
        fab.setOnClickListener(this);
        topicTextView = (TextView) findViewById(R.id.squad_details_topic_text);
        imageView1 = (ImageView) findViewById(R.id.squad_details_main_portrait1);
        imageView2 = (ImageView) findViewById(R.id.squad_details_main_portrait2);

        presenter.getSquad(squadKey);
    }

    @DebugLog
    @Override
    public void updateSquad(Squad squad) {
        switch (HpUtil.getStage(squad)) {
            case SUBMIT:
                showSquad(squad);
                break;
            case VOTE:
                startSquadVoteActivity(squad);
                finish();
                break;
            case RESULT:
                startActivity(new Intent(this, SquadResultActivity.class));
                finish();
                break;
            default:
                break;
        }

    }

    @DebugLog
    private void startSquadVoteActivity(Squad squad) {
        Intent intent = new Intent(SquadSubmitActivity.this, SquadVoteActivity.class);
        intent.putExtra(SELECTED_SQUAD_KEY, squad.getKey());
        startActivity(intent);
    }

    @DebugLog
    @Override
    public void showTopicsList(List<String> topics) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Choose a Topic!");
        alertDialog.setMessage(topics.get(2));
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                (dialog, which) -> {
                    dialog.dismiss();
                    topicSelected(topics.get(2), squadKey);
                });
        alertDialog.show();
    }


    @DebugLog
    public void showSquad(Squad squad) {
        if (squad.getMembers().get(0).getProfilePhotoUrl() != null) {


            //TODO mayke dynamic based on number of members.

            Glide.with(this)
                    .load(squad.getMembers().get(0).getProfilePhotoUrl())
                    .centerCrop()
                    .crossFade()
                    .bitmapTransform(new RoundImageTransform(this))
                    .into(imageView1);
        }

        if (squad.getMembers().size() > 1 && squad.getMembers().get(1).getProfilePhotoUrl() != null) {
            Glide.with(this)
                    .load(squad.getMembers().get(1).getProfilePhotoUrl())
                    .bitmapTransform(new RoundImageTransform(this))
                    .into(imageView2);
        }

        if (!squad.getTopic().isEmpty()) {
            topicTextView.setText(squad.getTopic());
        }
    }

    @DebugLog
    private void topicSelected(String topic, String squadKey) {
        startButton.setVisibility(View.INVISIBLE);
        presenter.setTopic(topic, squadKey);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.squad_details_start_button:
                presenter.getTopicsList();
                break;
            case R.id.squad_home_take_photo_fab:
                launchCamera();
        }
    }


    private static final int RC_TAKE_PICTURE = 101;
    private static final int RC_STORAGE_PERMS = 102;

    private Uri fileUri = null;

    @AfterPermissionGranted(RC_STORAGE_PERMS)
    private void launchCamera() {
        Log.d(TAG, "launchCamera");

        // Check that we have permission to read images from external storage.
        String perm = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        if (!EasyPermissions.hasPermissions(this, perm)) {
            EasyPermissions.requestPermissions(this, "Temp rationale", RC_STORAGE_PERMS, perm);
            return;
        }

        // Choose file storage location, must be listed in res/xml/file_paths.xml
        File dir = new File(Environment.getExternalStorageDirectory() + "/photos");
        File file = new File(dir, UUID.randomUUID().toString() + ".jpg");
        try {
            // Create directory if it does not exist.
            if (!dir.exists()) {
                dir.mkdir();
            }
            boolean created = file.createNewFile();
            Log.d(TAG, "file.createNewFile:" + file.getAbsolutePath() + ":" + created);
        } catch (IOException e) {
            Log.e(TAG, "file.createNewFile" + file.getAbsolutePath() + ":FAILED", e);
        }

        // Create content:// URI for file, required since Android N
        // See: https://developer.android.com/reference/android/support/v4/content/FileProvider.html
        fileUri = FileProvider.getUriForFile(this,
                "com.google.firebase.quickstart.firebasestorage.fileprovider", file);

        // Create and launch the intent
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // Grant permission to camera (this is required on KitKat and below)
        List<ResolveInfo> resolveInfos = getPackageManager()
                .queryIntentActivities(takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);
        for (ResolveInfo resolveInfo : resolveInfos) {
            String packageName = resolveInfo.activityInfo.packageName;
            grantUriPermission(packageName, fileUri,
                    Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }

        // Start picture-taking intent
        startActivityForResult(takePictureIntent, RC_TAKE_PICTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult:" + requestCode + ":" + resultCode + ":" + data);
        if (requestCode == RC_TAKE_PICTURE) {
            if (resultCode == RESULT_OK) {
                if (fileUri != null) {
                    uploadFromUri(fileUri);
                } else {
                    Log.w(TAG, "File URI is null");
                }
            } else {
                Toast.makeText(this, "Taking picture failed.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void uploadFromUri(Uri fileUri) {
        Log.d(TAG, "uploadFromUri:src:" + fileUri.toString());

        // Save the File URI
        this.fileUri = fileUri;

        // Toast message in case the user does not see the notification
        Toast.makeText(this, "Uploading...", Toast.LENGTH_SHORT).show();

        // Start MyUploadService to upload the file, so that the file is uploaded
        // even if this Activity is killed or put in the background
        uploadObservable.subscribe(aBoolean ->
                Toast.makeText(SquadSubmitActivity.this, aBoolean.toString(), Toast.LENGTH_SHORT).show());

        startService(new Intent(this, UploadService.class)
                .putExtra(UploadService.EXTRA_FILE_URI, fileUri)
                .putExtra(UploadService.SQUAD_KEY, squadKey)
                .setAction(UploadService.ACTION_UPLOAD));

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }


}
