package io.hotphotato.hotphotato.squadhome;

import dagger.Module;
import dagger.Provides;

/**
 * Created by mgaines on 12/7/16
 */
@Module
public class SquadSubmitPresenterModule {

    private SquadSubmitContract.View view;

    public SquadSubmitPresenterModule(SquadSubmitContract.View view) {
        this.view = view;
    }

    @Provides
    SquadSubmitContract.View providesSquadSubmitContractView() {
        return view;
    }

}
