package io.hotphotato.hotphotato.squadhome;


import java.util.List;

import javax.inject.Inject;

import hugo.weaving.DebugLog;
import io.hotphotato.hotphotato.firebase.HpDataBase;
import io.hotphotato.hotphotato.models.Squad;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by mgaines on 9/18/16
 */
class SquadSubmitPresenter implements SquadSubmitContract.Presenter {

    private SquadSubmitContract.View view;
    private HpDataBase hpDataBase;
    private CompositeSubscription compositeSubscription;

    @Inject
    public SquadSubmitPresenter(SquadSubmitContract.View view, HpDataBase hpDataBase) {
        this.view = view;
        this.hpDataBase = hpDataBase;
        compositeSubscription = new CompositeSubscription();
    }

    @DebugLog
    @Override
    public void getSquad(String squadKey) {
        compositeSubscription.add(
                hpDataBase.getSquad(squadKey)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<Squad>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onNext(Squad squad) {
                                view.updateSquad(squad);
                            }
                        }));
    }

    @DebugLog
    @Override
    public void getTopicsList() {
        compositeSubscription.add(
                hpDataBase.getTopics()
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<List<String>>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onNext(List<String> topics) {

                                view.showTopicsList(topics);
                            }
                        }));
    }

    @Override
    public void setTopic(String topic, String squadKey) {
        hpDataBase.setTopic(topic, squadKey);
    }

    @DebugLog
    @Override
    public void onDestroy() {
        compositeSubscription.unsubscribe();
    }

}
