package io.hotphotato.hotphotato.squadhome;


import java.util.List;

import io.hotphotato.hotphotato.models.Squad;

/**
 * Created by mgaines on 9/18/16
 */
public interface SquadSubmitContract {

    interface View {

        void updateSquad(Squad squad);

        void showTopicsList(List<String> topics);


    }

    interface Presenter {

        void getTopicsList();

        void setTopic(String topic, String squadKey);

        void onDestroy();

        void getSquad(String squadKey);
    }
}
