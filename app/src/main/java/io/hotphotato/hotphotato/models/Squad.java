package io.hotphotato.hotphotato.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mgaines on 10/2/16
 */
public class Squad {
    private String key;
    private String title;
    private String topic;
    private List<User> members = new ArrayList<>();

    // Empty constructor for Firebase.
    public Squad() {
    }

    //Constructor for creating squad but adding members later
    public Squad(String key, String title, String topic) {
        this.key = key;
        this.title = title;
        this.topic = topic;
    }

    public Squad(String key, String title, String topic, List<User> members) {
        this.key = key;
        this.title = title;
        this.topic = topic;
        this.members = members;
    }

    // Public getters & setters for Firebase

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<User> getMembers() {
        return members;
    }

    public void setMembers(List<User> members) {
        this.members = members;
    }

    public void addMember(User user) {
        members.add(user);
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

}
