package io.hotphotato.hotphotato.models;

/**
 * Created by mgaines on 9/16/16
 */
public class User {
    private String key;
    private String displayName;
    private String profilePhotoUrl;
    private String entryUrl;

    // Empty constructor for Firebase
    public User() {
    }

    public User(String key, String displayName, String profilePhotoUrl, String entryUrl) {
        this.key = key;
        this.displayName = displayName;
        if (profilePhotoUrl != null){
            this.profilePhotoUrl = profilePhotoUrl;
        }else{
            this.profilePhotoUrl = "";
        }
        if (entryUrl != null){
            this.entryUrl = entryUrl;
        }else{
            this.entryUrl = "";
        }

    }



    // Public Getters & Setters for Firebase


    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getProfilePhotoUrl() {
        return profilePhotoUrl;
    }

    public void setProfilePhotoUrl(String profilePhotoUrl) {
        this.profilePhotoUrl = profilePhotoUrl;
    }

    public String getEntryUrl() {
        return entryUrl;
    }

    public void setEntryUrl(String entryUrl) {
        this.entryUrl = entryUrl;
    }
}
