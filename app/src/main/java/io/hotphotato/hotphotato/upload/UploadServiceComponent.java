package io.hotphotato.hotphotato.upload;


import dagger.Component;
import io.hotphotato.hotphotato.ApplicationComponent;
import io.hotphotato.hotphotato.util.ActivityScoped;

/**
 * Created by mgaines on 12/7/16
 */
@ActivityScoped
@Component(dependencies = ApplicationComponent.class)
public interface UploadServiceComponent {
    void inject(UploadService uploadService);
}
