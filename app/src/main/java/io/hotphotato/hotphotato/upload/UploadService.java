package io.hotphotato.hotphotato.upload;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import javax.inject.Inject;
import javax.inject.Named;

import io.hotphotato.hotphotato.HpApplication;
import io.hotphotato.hotphotato.R;
import io.hotphotato.hotphotato.firebase.HpDataBase;
import io.hotphotato.hotphotato.login.LoginActivity;
import rx.subjects.PublishSubject;

/**
 * Created by mgaines on 11/14/16
 */

public class UploadService extends Service {

    private static final String TAG = UploadService.class.getSimpleName();
    private int mNumTasks = 0;
    private static final int NOTIF_ID_DOWNLOAD = 0;

    /**
     * Intent Actions
     **/
    public static final String ACTION_UPLOAD = "action_upload";
    public static final String UPLOAD_COMPLETED = "upload_completed";
    public static final String UPLOAD_ERROR = "upload_error";
    /**
     * Intent Extras
     **/
    public static final String EXTRA_FILE_URI = "extra_file_uri";
    public static final String EXTRA_DOWNLOAD_URL = "extra_download_url";
    public static final String SQUAD_KEY = "squad_key";

    // [START declare_ref]
    private StorageReference storageRef;
    // [END declare_ref]

    @Inject
    @Named("upload_result")
    PublishSubject<Boolean> uploadObservable;

    @Inject
    HpDataBase hpDataBase;


    public void taskStarted() {
        changeNumberOfTasks(1);
    }

    public void taskCompleted() {
        changeNumberOfTasks(-1);
    }

    private synchronized void changeNumberOfTasks(int delta) {
        Log.d(TAG, "changeNumberOfTasks:" + mNumTasks + ":" + delta);
        mNumTasks += delta;

        // If there are no tasks left, stop the service
        if (mNumTasks <= 0) {
            Log.d(TAG, "stopping");
            stopSelf();
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        DaggerUploadServiceComponent.builder()
                .applicationComponent(((HpApplication) getApplication()).getApplicationComponent())
                .build()
                .inject(this);


        // [START get_storage_ref]
        storageRef = FirebaseStorage.getInstance().getReference();
        // [END get_storage_ref]
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand:" + intent + ":" + startId);
        if (ACTION_UPLOAD.equals(intent.getAction())) {
            Uri fileUri = intent.getParcelableExtra(EXTRA_FILE_URI);
            String squadKey = intent.getStringExtra(SQUAD_KEY);
            uploadFromUri(fileUri, squadKey);
        }

        return START_REDELIVER_INTENT;
    }

    // [START upload_from_uri]
    private void uploadFromUri(final Uri fileUri, final String squadKey) {
        Log.d(TAG, "uploadFromUri:src:" + fileUri.toString());

        // [START_EXCLUDE]
        taskStarted();
        showUploadProgressNotification();
        // [END_EXCLUDE]

        // [START get_child_ref]
        // Get a reference to store file at photos/<FILENAME>.jpg
        final StorageReference photoRef = storageRef.child("photos")
                .child(fileUri.getLastPathSegment());
        // [END get_child_ref]

        // Upload file to Firebase Storage
        Log.d(TAG, "uploadFromUri:dst:" + photoRef.getPath());
        photoRef.putFile(fileUri)
                .addOnSuccessListener(taskSnapshot -> {
                    // Upload succeeded
                    Log.d(TAG, "uploadFromUri:onSuccess");
                    uploadObservable.onNext(true);


                    // Get the public download URL
                    Uri downloadUri = taskSnapshot.getMetadata().getDownloadUrl();
                    hpDataBase.setPhotoEntry(squadKey, downloadUri.toString());

                    // [START_EXCLUDE]
                    broadcastUploadFinished(downloadUri, fileUri);
                    showUploadFinishedNotification(downloadUri, fileUri);
                    taskCompleted();
                    // [END_EXCLUDE]
                })
                .addOnFailureListener(exception -> {
                    // Upload failed
                    Log.w(TAG, "uploadFromUri:onFailure", exception);
                    uploadObservable.onNext(false);

                    // [START_EXCLUDE]
                    broadcastUploadFinished(null, fileUri);
                    showUploadFinishedNotification(null, fileUri);
                    taskCompleted();
                    // [END_EXCLUDE]
                });
    }
    // [END upload_from_uri]

    /**
     * Broadcast finished upload (success or failure).
     *
     * @return true if a running receiver received the broadcast.
     */
    private boolean broadcastUploadFinished(@Nullable Uri downloadUrl, @Nullable Uri fileUri) {
        boolean success = downloadUrl != null;

        String action = success ? UPLOAD_COMPLETED : UPLOAD_ERROR;

        Intent broadcast = new Intent(action)
                .putExtra(EXTRA_DOWNLOAD_URL, downloadUrl)
                .putExtra(EXTRA_FILE_URI, fileUri);
        return LocalBroadcastManager.getInstance(getApplicationContext())
                .sendBroadcast(broadcast);
    }

    /**
     * Show a notification for a finished upload.
     */
    private void showUploadFinishedNotification(@Nullable Uri downloadUrl, @Nullable Uri fileUri) {
        // Make Intent to LoginActivity
        Intent intent = new Intent(this, LoginActivity.class)
                .putExtra(EXTRA_DOWNLOAD_URL, downloadUrl)
                .putExtra(EXTRA_FILE_URI, fileUri)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        // Make PendingIntent for notification
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* requestCode */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        // Set message and icon based on success or failure
        boolean success = downloadUrl != null;
        String message = success ? "Sending finished" : "Sending failed";
        int icon = success ? R.mipmap.ic_launcher : R.mipmap.ic_launcher;

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(icon)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(message)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);

        NotificationManager manager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        manager.notify(NOTIF_ID_DOWNLOAD, builder.build());
    }

    /**
     * Show notification with an indeterminate upload progress bar.
     */
    private void showUploadProgressNotification() {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getString(R.string.app_name))
                .setContentText("Sending...")
                .setProgress(0, 0, true)
                .setOngoing(true)
                .setAutoCancel(false);

        NotificationManager manager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        manager.notify(NOTIF_ID_DOWNLOAD, builder.build());
    }

    public static IntentFilter getIntentFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(UPLOAD_COMPLETED);
        filter.addAction(UPLOAD_ERROR);

        return filter;
    }

}
