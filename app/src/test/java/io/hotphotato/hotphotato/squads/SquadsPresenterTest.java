package io.hotphotato.hotphotato.squads;


import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

import io.hotphotato.hotphotato.firebase.HpDataBase;
import io.hotphotato.hotphotato.models.Squad;
import io.hotphotato.hotphotato.util.HpUtil;
import io.hotphotato.hotphotato.util.schedulers.BaseSchedulerProvider;
import io.hotphotato.hotphotato.util.schedulers.ImmediateSchedulerProvider;
import rx.Observable;
import rx.Subscriber;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by mgaines on 12/27/16
 */
public class SquadsPresenterTest {

    @Mock
    private SquadsContract.View view;

    @Mock
    private HpDataBase hpDataBase;

    @Mock
    private HpUtil hpUtil;

    @Captor
    private ArgumentCaptor<HashMap<String, Squad>> squadMap;

    private BaseSchedulerProvider baseSchedulerProvider;

    private SquadsPresenter presenter;


    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);

        baseSchedulerProvider = new ImmediateSchedulerProvider();

        presenter = new SquadsPresenter(view, hpDataBase, baseSchedulerProvider, hpUtil);


    }

    @Test
    public void getSquadsReturnsEmptyShowEmptyState() {

        when(hpDataBase.getSquadKeys()).thenReturn(Observable.just(Collections.emptyList()));

        presenter.getSquads();

        verify(view).showLoadingIndicator(true);
        verify(view).showLoadingIndicator(false);
        verify(view).showEmptyDisplay();

        verify(view, never()).setSquads(any());

    }

    @Test
    public void getSquadsAndSendSquadsToView() {

        when(hpDataBase.getSquadKeys())
                .thenReturn(Observable.just(new ArrayList<>(Arrays.asList("1"))));
        when(hpDataBase.getSquad("1")).thenReturn(Observable.just(new Squad("1", "", "")));

        presenter.getSquads();

        verify(view).showLoadingIndicator(true);
        verify(view, atLeastOnce()).showLoadingIndicator(false);
        verify(view, atLeastOnce()).setSquads(squadMap.capture());

    }

    @Test
    public void getSquadsAndSendUniqueSquadsToView() {

        when(hpDataBase.getSquadKeys())
                .thenReturn(Observable.just(new ArrayList<>(Arrays.asList("1", "2", "1"))));
        when(hpDataBase.getSquad("1")).thenReturn(Observable.just(new Squad("1", "", "")));
        when(hpDataBase.getSquad("2")).thenReturn(Observable.just(new Squad("2", "", "")));
        //Firebase sends an update for squad 1
        when(hpDataBase.getSquad("1")).thenReturn(Observable.just(new Squad("1", "", "")));

        presenter.getSquads();

        verify(view).showLoadingIndicator(true);
        verify(view, atLeastOnce()).showLoadingIndicator(false);
        verify(view, atLeastOnce()).setSquads(squadMap.capture());

        assertEquals(true, squadMap.getValue().containsKey("1"));
        assertEquals(true, squadMap.getValue().containsKey("2"));
        assertEquals(2, squadMap.getValue().size());

    }

    @Test
    public void getSquadsHasFirebaseErrorAndSetsErrorView() {
        final String TEST_ERROR = "test_error";

        Observable<String> observable = Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onError(new Throwable(TEST_ERROR));
            }
        });

        when(hpDataBase.getSquadKeys()).thenReturn(observable.toList());

        presenter.getSquads();

        verify(view).showLoadingIndicator(true);
        verify(view).showLoadingIndicator(false);
        verify(view).showErrorScreen();

    }

    @Test
    public void createNewSquadAndAddCurrentUserToIt() {
        final String TEST_KEY = "testkey";
        final String TEST_DEEPLINK = "https://MyDeepLink.com";

        when(hpDataBase.createNewSquad(anyString())).thenReturn(Observable.just(TEST_KEY));
        when(hpUtil.buildDeepLink(anyString())).thenReturn(TEST_DEEPLINK);

        presenter.createNewSquad("");

        verify(view).showLoadingIndicator(true);
        verify(view, atLeastOnce()).showLoadingIndicator(false);
        verify(hpDataBase).addCurrentUserToSquad(TEST_KEY);
        verify(view).sendDeepLink(TEST_DEEPLINK);

    }

    @Test
    public void createNewSquadHasFirebaseErrorAndSetsErrorView() {
        final String TEST_ERROR = "test_error";

        Observable<String> observable = Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onError(new Throwable(TEST_ERROR));
            }
        });

        when(hpDataBase.createNewSquad(anyString())).thenReturn(observable);

        presenter.createNewSquad("");

        verify(view).showLoadingIndicator(true);
        verify(view).showLoadingIndicator(false);
        verify(view).showErrorScreen();

    }


}