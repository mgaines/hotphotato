package io.hotphotato.hotphotato.login;


import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.hotphotato.hotphotato.firebase.HpDataBase;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by mgaines on 12/23/16
 */

public class LoginPresenterTest {

    @Mock
    LoginContract.View view;

    @Mock
    HpDataBase hpDataBase;

    private LoginPresenter presenter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        presenter = new LoginPresenter(view, hpDataBase);
    }

    @Test
    public void loginWhenUserIsNotLoggedIn() {
        when(hpDataBase.isUserLoggedIn()).thenReturn(false);

        presenter.login();

        verify(view).displaySignIn();
    }

    @Test
    public void loginWhenUserIsLoggedIn() {
        when(hpDataBase.isUserLoggedIn()).thenReturn(true);

        presenter.login();

        verify(hpDataBase).updateUser();
        verify(view).processDeepLink();
    }

    @Test
    public void addUserToSquad() {
        presenter.addUserToSquad("squad_key");

        verify(hpDataBase).addCurrentUserToSquad("squad_key");
    }
}